# Emotionify
Project of Principles of Software Design Course. (Amirkabir University of Technology / Spring 98-99)


### add all defined jobs from CRONJOBS to crontab:
```shell script
python3 manage.py crontab add
```
### show current active jobs:
```shell script
python3 manage.py crontab show
```
### removing all defined jobs:
```shell script
python3 manage.py crontab remove
```
