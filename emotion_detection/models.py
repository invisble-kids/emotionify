from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models

from Emotionify.settings import Emotion, MusicTag


class SpotifyUser(AbstractBaseUser):
    spotify_id = models.CharField(max_length=64, unique=True)
    refresh_token = models.CharField(max_length=512)
    display_name = models.CharField(max_length=128)
    email = models.EmailField()

    USERNAME_FIELD = 'spotify_id'


class ImageModel(models.Model):
    image = models.ImageField()


class MusicModel(models.Model):
    MODE_CHOICES = (
        (0, 'minor'),
        (1, 'major')
    )

    spotify_id = models.CharField(max_length=63, unique=True, null=True)
    name = models.CharField(max_length=63, null=True)
    artist = models.CharField(max_length=63, null=True)
    album = models.CharField(max_length=63, null=True)
    release_date = models.DateField(null=True)
    cover = models.URLField(null=True)
    length = models.FloatField(null=True)
    url = models.URLField(null=True)

    track_key = models.IntegerField(null=True)
    mode = models.IntegerField(choices=MODE_CHOICES, null=True)
    time_signature = models.IntegerField(null=True)
    acousticness = models.FloatField(null=True)
    danceability = models.FloatField(null=True)
    energy = models.FloatField(null=True)
    instrumentalness = models.FloatField(null=True)
    liveness = models.FloatField(null=True)
    loudness = models.FloatField(null=True)
    speechiness = models.FloatField(null=True)
    valence = models.FloatField(null=True)
    tempo = models.FloatField(null=True)
    music_tag = models.CharField(max_length=15, choices=MusicTag.choices(), null=True)

    created_at = models.DateTimeField(auto_now_add=True)


class EmotionEntityModel(models.Model):
    user = models.ForeignKey(SpotifyUser, on_delete=models.PROTECT, related_name='emotion_images')
    emotion = models.CharField(max_length=15, choices=Emotion.choices(), null=True)
    playlist_id = models.CharField(max_length=31, unique=True, null=True)
    musics = models.ManyToManyField(MusicModel)
    created_at = models.DateTimeField(auto_now_add=True)


class FeedbackModel(models.Model):
    STATE_CHOICES = (
        ('like', 'like'),
        ('dislike', 'dislike')
    )

    user = models.ForeignKey(SpotifyUser, on_delete=models.PROTECT, related_name="feedbacks")
    playlist_id = models.CharField(max_length=31, null=True)
    music = models.ForeignKey(MusicModel, on_delete=models.PROTECT, related_name="feedbacks")
    state = models.CharField(choices=STATE_CHOICES, max_length=7)
    created_at = models.DateTimeField(auto_now_add=True)


class MusicPreferenceModel(models.Model):
    FEELING_CHOICES = (
        (MusicTag.HAPPY, MusicTag.HAPPY.value),
        (MusicTag.SAD, MusicTag.SAD.value),
        ('default', 'default')
    )
    TYPE_CHOICES = (
        (MusicTag.ENERGETIC, MusicTag.ENERGETIC.value),
        (MusicTag.CALM, MusicTag.CALM.value),
        ('default', 'default')
    )

    user = models.CharField(max_length=63)
    emotion = models.CharField(max_length=15, choices=Emotion.choices())
    prefered_feeling = models.CharField(max_length=15, default='default', choices=FEELING_CHOICES)
    prefered_type = models.CharField(max_length=15, default='default', choices=TYPE_CHOICES)
