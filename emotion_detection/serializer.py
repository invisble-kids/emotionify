from rest_framework import serializers

from emotion_detection.models import ImageModel


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImageModel
        fields = ('image',)
