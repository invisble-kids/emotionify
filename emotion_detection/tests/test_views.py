import json
from unittest import mock

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from emotion_detection.models import SpotifyUser, EmotionEntityModel


class UserTestCase(TestCase):
    def setUp(self):
        self.sp_user_data = {
            'spotify_id': '123456789',
            'display_name': 'testusername',
            'refresh_token': 'testtest1234Y',
            'email': 'test@test.com',
        }

        self.user, created = SpotifyUser.objects.update_or_create(
            spotify_id=self.sp_user_data['spotify_id'],
            defaults={
                'display_name': self.sp_user_data['display_name'],
                'email': self.sp_user_data['email'],
                'refresh_token': self.sp_user_data['refresh_token']
            },
        )
        self.token, created = Token.objects.get_or_create(user=self.user)

    def test_validate_user(self):
        self.assertIsNotNone(self.user)
        self.assertEqual(self.user.spotify_id, self.sp_user_data['spotify_id'])
        self.assertEqual(self.user.display_name, self.sp_user_data['display_name'])
        self.assertEqual(self.user.email, self.sp_user_data['email'])
        self.assertEqual(self.user.refresh_token, self.sp_user_data['refresh_token'])
        self.assertIsNotNone(self.token)


class SpotifyConnectTestCase(UserTestCase):

    def setUp(self):
        super().setUp()
        self.client = APIClient()
        self.spotify_url = '/spotify-connect/'

    def test_get_register_url(self):
        resp = self.client.get(self.spotify_url)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIn('register_url', resp.data)
        self.assertIsNotNone(resp.data.get('register_url'))


class ImageRecommendTestCase(UserTestCase):

    def setUp(self):
        super().setUp()
        self.client = APIClient()
        self.image_url = '/recommend/image/'

    @mock.patch('emotion_detection.views.get_playlist_from_emotion', return_value='7635419230', autospec=True)
    def test_login_user(self, mock_playlist_id):
        image = SimpleUploadedFile(
            "sadface.jpg", content=open('emotion_detection/tests/sadface.jpg', 'rb').read(), content_type="image/jpg")
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + str(self.token))
        resp = self.client.post(self.image_url, {"image": image})
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIn('playlist_id', resp.data)
        self.assertIsNotNone(resp.data.get('playlist_id'))
        self.assertEqual(EmotionEntityModel.objects.all().count(), 1)

        history = EmotionEntityModel.objects.filter(user=self.user).values()[0]
        self.assertIn('user_id', history)
        self.assertIn('emotion', history)
        self.assertEqual(history['emotion'], 'sadness')
        self.assertIn('playlist_id', history)
        self.assertIn('created_at', history)

    @mock.patch('emotion_detection.views.get_playlist_from_emotion', return_value='7635419230', autospec=True)
    def test_anonymous_user(self, mock_playlist_id):
        image = SimpleUploadedFile(
            "sadface.jpg", content=open('emotion_detection/tests/sadface.jpg', 'rb').read(), content_type="image/jpg")
        resp = self.client.post(self.image_url, {"image": image})
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIn('playlist_id', resp.data)
        self.assertIsNotNone(resp.data.get('playlist_id'))
        self.assertEqual(EmotionEntityModel.objects.all().count(), 0)

    def test_invalid_image(self):
        image = SimpleUploadedFile(
            "noface.png", content=open('emotion_detection/tests/noface.png', 'rb').read(), content_type="image/png")
        resp = self.client.post(self.image_url, {'image': image}, format='multipart')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)


class EmotionRecommendTestCase(UserTestCase):

    def setUp(self):
        super().setUp()
        self.client = APIClient()
        self.emotion_url = '/recommend/emotion/'

    @mock.patch('emotion_detection.views.get_playlist_from_emotion', return_value='7635419230', autospec=True)
    def test_login_user(self, mock_playlist_id):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + str(self.token))
        resp = self.client.post(self.emotion_url, json.dumps({"emotion": "happiness"}), content_type='application/json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIn('playlist_id', resp.data)
        self.assertIsNotNone(resp.data.get('playlist_id'))
        self.assertEqual(EmotionEntityModel.objects.all().count(), 1)

        history = EmotionEntityModel.objects.filter(user=self.user).values()[0]
        self.assertIn('user_id', history)
        self.assertIn('emotion', history)
        self.assertEqual(history['emotion'], 'happiness')
        self.assertIn('playlist_id', history)
        self.assertIn('created_at', history)

    @mock.patch('emotion_detection.views.get_playlist_from_emotion', return_value='7635419230', autospec=True)
    def test_anonymous_user(self, mock_playlist_id):
        resp = self.client.post(self.emotion_url, json.dumps({"emotion": "sadness"}), content_type='application/json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIn('playlist_id', resp.data)
        self.assertIsNotNone(resp.data.get('playlist_id'))
        self.assertEqual(EmotionEntityModel.objects.all().count(), 0)

    def test_invalid_emotion(self):
        resp = self.client.post(self.emotion_url, json.dumps({"emotion": "sad"}), content_type='application/json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)

        resp = self.client.post(self.emotion_url, json.dumps({"something": "else"}), content_type='application/json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)


class ProfileTestCase(UserTestCase):

    def setUp(self):
        super().setUp()
        self.client = APIClient()
        self.emotion_url = '/recommend/emotion/'
        self.profile_url = '/profile/'

    @mock.patch('emotion_detection.views.get_playlist_from_emotion', return_value='7635419230', autospec=True)
    def test_login_user(self, mock_playlist_id):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + str(self.token))
        self.client.post(self.emotion_url, json.dumps({"emotion": "happiness"}), content_type='application/json')
        resp = self.client.get(self.profile_url)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIn('request_history', resp.data)
        self.assertIsNotNone(resp.data.get('request_history'))
        self.assertEqual(len(resp.data.get('request_history')), 1)

        history = resp.data.get('request_history')[0]
        self.assertIn('user', history)
        self.assertEqual(history['user'], self.user.display_name)
        self.assertIn('playlist_id', history)
        self.assertIn('created_at', history)

    def test_login_user_no_history(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + str(self.token))
        resp = self.client.get(self.profile_url)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIn('request_history', resp.data)
        self.assertIsNotNone(resp.data.get('request_history'))
        self.assertEqual(len(resp.data.get('request_history')), 0)

    def test_anonymous_user(self):
        resp = self.client.get(self.profile_url)
        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)


class LikePlaylistTestCase(UserTestCase):

    def setUp(self):
        super().setUp()
        self.client = APIClient()
        self.like_playlist_url = '/like/playlist/'

    def test_anonymous_user(self):
        resp = self.client.get(self.like_playlist_url)
        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_invalid_message(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + str(self.token))
        resp = self.client.post(self.like_playlist_url, json.dumps({"playlist_id": "not_exist"}),
                                content_type='application/json')
        self.assertEqual(resp.status_code, status.HTTP_404_NOT_FOUND)

        resp = self.client.post(self.like_playlist_url, json.dumps({"something": "else"}),
                                content_type='application/json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)


class LikeTrackTestCase(UserTestCase):

    def setUp(self):
        super().setUp()
        self.client = APIClient()
        self.like_track_url = '/like/track/'

    def test_anonymous_user(self):
        resp = self.client.get(self.like_track_url)
        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_invalid_message(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + str(self.token))
        resp = self.client.post(self.like_track_url, json.dumps({"playlist_id": "134358rth5ud"}),
                                content_type='application/json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)

        resp = self.client.post(self.like_track_url, json.dumps({"track_index": "23c4x6v76ye2"}),
                                content_type='application/json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)

        resp = self.client.post(self.like_track_url, json.dumps({"something": "else"}),
                                content_type='application/json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
