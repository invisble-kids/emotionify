import json
import os

from django.test import TestCase

from emotion_detection.tasks import *


class TasksTest(TestCase):
    def test_get_spotify_playlists(self):
        limit, offset = 1, 23
        spotify_playlists = get_spotify_playlists(limit=limit, offset=offset)
        playlists = spotify_playlists['playlists']
        next_limit, next_offset = spotify_playlists['next']['limit'], spotify_playlists['next']['offset']
        self.assertIsNotNone(next_limit)
        self.assertIsNotNone(next_offset)
        self.assertEqual(limit, len(playlists))

        limit, offset = 7, 123456
        spotify_playlists = get_spotify_playlists(limit=limit, offset=offset)
        playlists = spotify_playlists['playlists']
        next_limit, next_offset = spotify_playlists['next']['limit'], spotify_playlists['next']['offset']
        self.assertIsNone(next_limit)
        self.assertIsNone(next_offset)
        self.assertEqual(0, len(playlists))

    def test_get_artist_albums(self):
        with open(os.path.join(os.path.dirname(__file__), 'artist.json'), 'r') as json_file:
            artist = json.load(json_file)

        artist_albums = get_artist_albums(artist)

        with open(os.path.join(os.path.dirname(__file__), 'artist_albums.json'), 'r') as json_file:
            expected_artist_albums = json.load(json_file)

        self.assertEqual(len(artist_albums), len(expected_artist_albums))

    def test_get_related_artists(self):
        with open(os.path.join(os.path.dirname(__file__), 'artist.json'), 'r') as json_file:
            artist = json.load(json_file)

        related_artists = get_related_artists(artist, 5, 3)

        self.assertLessEqual(1 + 5 + 25, len(related_artists))
        self.assertLessEqual(len(related_artists), 1 + 5 + 25 + 125)

    def test_get_music_tag(self):
        with open(os.path.join(os.path.dirname(__file__), 'track_features.json'), 'r') as json_file:
            track_features = json.load(json_file)

        music_tag = get_music_tag(track_features)

        self.assertEqual(music_tag.value, 'Sad')
