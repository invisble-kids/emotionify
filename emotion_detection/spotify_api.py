import spotipy

from Emotionify.settings import SPOTIFY


class SpotifyClient:
    def __init__(self):
        invisible_kids_oauth = spotipy.oauth2.SpotifyOAuth(
            SPOTIFY.get('CLIENT_ID'), SPOTIFY.get('CLIENT_SECRET'),
            redirect_uri=SPOTIFY.get('API_REDIRECT_URI'),
            scope='playlist-read-private playlist-modify-private',
            cache_path='.invisibleKids-token.cache',
            show_dialog=False
        )

        token = invisible_kids_oauth.get_cached_token()
        if token is not None:
            self.client = spotipy.Spotify(auth=token['access_token'])
        else:
            raise Exception('spotify token invalid')

    def create_playlist(self, playlist_musics):
        playlist = self.client.user_playlist_create(
            user=SPOTIFY.get("API_USER_ID"),
            name='Your Recommended Playlist',
            public=False
        )
        self.client.user_playlist_add_tracks(
            user=SPOTIFY.get("API_USER_ID"),
            playlist_id=playlist['id'],
            tracks=playlist_musics
        )
        return playlist
