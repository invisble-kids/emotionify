from django.urls import path

from emotion_detection.views import *

urlpatterns = [
    path('spotify-connect/', SpotifyConnect.as_view(), name='login'),
    path('profile/', UserProfile.as_view(), name='profile'),
    path('recommend/image/', ImageRecommendation.as_view(), name='recommend_image'),
    path('recommend/emotion/', EmotionRecommendation.as_view(), name='recommend_emotion'),
    path('like/playlist/', LikePlaylist.as_view(), name='like_playlist'),
    path('like/track/', LikeTrack.as_view(), name='like_track'),
    path('liked-tracks/', LikedTracks.as_view(), name='liked_tracks'),
    path('profile/preferences/', Preferences.as_view(), name='preferences'),
]
