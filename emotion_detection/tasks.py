import spotipy

from Emotionify.settings import SPOTIFY, MUSIC_TAGS_MEAN_FEATURES, PLAYLIST_LIMIT, LAYERS_RELATED, TOP_N_RELATED, \
    ALBUM_LIMIT
from emotion_detection.models import MusicModel

client_credentials_manager = spotipy.SpotifyClientCredentials(
    client_id=SPOTIFY['CLIENT_ID'],
    client_secret=SPOTIFY['CLIENT_SECRET']
)
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)


def get_spotify_playlists(limit, offset):
    spotify_playlists = sp.user_playlists('spotify', limit=limit, offset=offset)
    playlists = [sp.playlist(spotify_playlist['uri']) for spotify_playlist in spotify_playlists['items']]

    if spotify_playlists['next'] is None:
        next_offset, next_limit = None, None
    else:
        next_offset, next_limit = tuple(
            [part.split('=')[-1] for part in spotify_playlists['next'].split('?')[-1].split('&')]
        )

    return {
        'next': {'limit': next_limit, 'offset': next_offset},
        'playlists': playlists
    }


def get_artist_albums(artist):
    all_albums = []

    limit, offset = ALBUM_LIMIT, 0
    while True:
        artist_albums = sp.artist_albums(artist['uri'], limit=limit, offset=offset)
        all_albums.extend(sp.albums([artist_album['uri'] for artist_album in artist_albums['items']])['albums'])

        if artist_albums['next'] is None:
            break
        offset += limit

    return all_albums


def get_related_artists(artist, top_n, layers):
    all_artists = []

    related_artists = [[] for _ in range(layers + 1)]
    related_artists[0] = [artist]
    all_artists.extend(related_artists[0])

    for layer in range(1, layers + 1):
        for layer_related_artist in related_artists[layer - 1]:
            layer_related_related_artists = sp.artist_related_artists(layer_related_artist['uri'])['artists'][:top_n]
            for layer_related_related_artist in layer_related_related_artists:
                if layer_related_related_artist not in all_artists and \
                        layer_related_related_artist not in related_artists[layer]:
                    related_artists[layer].append(layer_related_related_artist)
        all_artists.extend(related_artists[layer])

    return all_artists


def get_music_tag(track_features):
    energy, valence, tempo = track_features['energy'], track_features['valence'], track_features['tempo']

    # intensity value between 0 and 0.5 and energy value between 0 and 1
    # pitch value between 0 and 1000 and valence value between 0 and 1
    intensity, pitch, rhythm = energy / 2, valence * 1000, tempo
    diffs = [
        (((intensity - val[0]) ** 2 + (pitch - val[1]) ** 2 + (rhythm - val[2]) ** 2) ** 0.5, key)
        for key, val in MUSIC_TAGS_MEAN_FEATURES.items()
    ]

    return sorted(diffs)[0][1]


def add_albums_to_db(albums):
    for album in albums:
        tracks = album['tracks']['items']
        tracks_features = sp.audio_features([track['uri'] for track in tracks])

        for track, track_features in zip(tracks, tracks_features):
            try:
                MusicModel.objects.get(spotify_id=track['id'])
            except MusicModel.DoesNotExist:
                try:
                    music_tag = get_music_tag(track_features)

                    MusicModel.objects.create(
                        spotify_id=track['id'],
                        name=track['name'],
                        artist=', '.join(artist['name'] for artist in track['artists']),
                        album=album['name'],
                        release_date=album['release_date'],
                        cover=album['images'][1]['url'],
                        length=track['duration_ms'] / 1000,
                        url=track['external_urls']['spotify'],
                        track_key=track_features['key'],
                        mode=track_features['mode'],
                        time_signature=track_features['time_signature'],
                        acousticness=track_features['acousticness'],
                        danceability=track_features['danceability'],
                        energy=track_features['energy'],
                        instrumentalness=track_features['instrumentalness'],
                        liveness=track_features['liveness'],
                        loudness=track_features['loudness'],
                        speechiness=track_features['speechiness'],
                        valence=track_features['valence'],
                        tempo=track_features['tempo'],
                        music_tag=music_tag.value
                    )
                except:
                    pass


def deep_search_task():
    limit, offset = PLAYLIST_LIMIT, 0

    crawled_artist_name = set()
    while limit is not None and offset is not None:
        result = get_spotify_playlists(limit, offset)

        for playlist in result['playlists']:
            tracks = playlist['tracks']['items']
            for track in tracks:
                artists = track['track']['artists']
                for artist in artists:
                    if artist['name'] not in crawled_artist_name:
                        related_artists = get_related_artists(artist, TOP_N_RELATED, LAYERS_RELATED)
                        for related_artist in related_artists:
                            if related_artist['name'] not in crawled_artist_name:
                                artist_albums = get_artist_albums(related_artist)
                                add_albums_to_db(artist_albums)
                                crawled_artist_name.add(related_artist['name'])

        limit, offset = result['next']['limit'], result['next']['offset']
