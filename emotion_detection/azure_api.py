import operator

from azure.cognitiveservices.vision.face import FaceClient
from msrest.authentication import CognitiveServicesCredentials

from Emotionify.settings import AZURE_API_KEY, AZURE_API_ENDPOINT, Emotion


class AzureClient:
    def __init__(self):
        self.credentials = CognitiveServicesCredentials(AZURE_API_KEY)
        self.face_client = FaceClient(AZURE_API_ENDPOINT, self.credentials)

    def get_emotion_from_image(self, image):
        detected_faces = self.face_client.face.detect_with_stream(image, return_face_attributes=['emotion'])
        if len(detected_faces) == 0:
            return None
        emotions = vars(detected_faces[0].face_attributes.emotion)
        emotions.pop('additional_properties')
        strongest_emotion = max(emotions.items(), key=operator.itemgetter(1))[0]
        return Emotion(strongest_emotion)
