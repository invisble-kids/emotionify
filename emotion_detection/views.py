import datetime
import json
import random

import spotipy
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ParseError, NotFound
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from Emotionify.settings import SPOTIFY, EMOTION_TO_MUSIC_TAGS, RECOMMENDATION_SIZE, Emotion, EMOTION_SYNONYMS, \
    ANIMAL_NAMES, PREFERENCE_FEELING_TYPE_CASES
from emotion_detection.azure_api import AzureClient
from emotion_detection.models import EmotionEntityModel, MusicModel, SpotifyUser, FeedbackModel, MusicPreferenceModel
from emotion_detection.serializer import ImageSerializer
from emotion_detection.spotify_api import SpotifyClient


class SpotifyConnect(APIView):
    sp_oauth = spotipy.oauth2.SpotifyOAuth(
        client_id=SPOTIFY.get('CLIENT_ID'),
        client_secret=SPOTIFY.get('CLIENT_SECRET'),
        redirect_uri=SPOTIFY.get('REDIRECT_URI'),
        scope=SPOTIFY.get('ACCESS_SCOPE')
    )

    def get(self, request):
        auth_url = self.sp_oauth.get_authorize_url()
        return Response({"register_url": auth_url}, status=status.HTTP_200_OK)

    def post(self, request):
        url = request.data.get('code')
        code = self.sp_oauth.parse_response_code(url)
        token_info = self.sp_oauth.get_access_token(code, check_cache=False)
        sp = spotipy.Spotify(auth=token_info['access_token'])
        sp_user_data = sp.current_user()
        user, created = SpotifyUser.objects.update_or_create(
            spotify_id=sp_user_data['id'],
            defaults={
                'display_name': sp_user_data['display_name'],
                'email': sp_user_data['email'],
                'refresh_token': token_info['refresh_token']
            },
        )
        token, created = Token.objects.get_or_create(user=user)

        return Response({"token": token.key}, status=status.HTTP_200_OK)


class UserProfile(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user_history = EmotionEntityModel.objects.filter(user=request.user)
        result = []
        for history in user_history[::-1]:
            result.append({
                'user': request.user.display_name,
                'created_at': history.created_at.strftime("%Y-%m-%d %H:%M:%S"),
                'playlist_id': history.playlist_id
            })
        return Response({'request_history': result}, status=status.HTTP_200_OK)


def get_prefered_tags(user_id, emotion):
    if user_id is None:
        return EMOTION_TO_MUSIC_TAGS[emotion]

    preferences = MusicPreferenceModel.objects.filter(user=user_id, emotion=emotion.value)
    if not preferences:
        return EMOTION_TO_MUSIC_TAGS[emotion]

    last_preference = preferences[0]
    return PREFERENCE_FEELING_TYPE_CASES[(emotion, last_preference.prefered_feeling, last_preference.prefered_type)]


def get_playlist_from_emotion(user_id, emotion):
    prefered_tags = get_prefered_tags(user_id, emotion)
    recommended_musics = MusicModel.objects.filter(
        music_tag__in=[music_tag.value for music_tag in prefered_tags]
    ).order_by("?")[:RECOMMENDATION_SIZE]
    musics_id = [music.spotify_id for music in recommended_musics]
    playlist = SpotifyClient().create_playlist(musics_id)
    return playlist['id']


class ImageRecommendation(APIView):
    def post(self, request):
        file_serializer = ImageSerializer(data=request.data)

        if not file_serializer.is_valid():
            raise ParseError(detail=json.dumps(file_serializer.errors))
        file_serializer.save()

        image_path = file_serializer.data.get('image')
        with open(image_path, 'rb') as image:
            emotion = AzureClient().get_emotion_from_image(image)

        if emotion is None:
            raise ParseError

        if request.user.is_authenticated:
            playlist_id = get_playlist_from_emotion(request.user, emotion)
        else:
            playlist_id = get_playlist_from_emotion(None, emotion)

        if request.user.is_authenticated:
            EmotionEntityModel.objects.create(
                user=request.user,
                emotion=emotion.value,
                playlist_id=playlist_id
            )

        return Response({'playlist_id': playlist_id}, status=status.HTTP_200_OK)


class EmotionRecommendation(APIView):
    def post(self, request):
        emotion_str = request.data.get('emotion')

        try:
            emotion = Emotion(emotion_str)
        except:
            raise ParseError

        if request.user.is_authenticated:
            playlist_id = get_playlist_from_emotion(request.user, emotion)
        else:
            playlist_id = get_playlist_from_emotion(None, emotion)

        if request.user.is_authenticated:
            EmotionEntityModel.objects.create(
                user=request.user,
                emotion=emotion.value,
                playlist_id=playlist_id
            )

        return Response({'playlist_id': playlist_id}, status=status.HTTP_200_OK)


class LikePlaylist(APIView):
    permission_classes = [IsAuthenticated]

    sp_oauth = spotipy.oauth2.SpotifyOAuth(
        client_id=SPOTIFY.get('CLIENT_ID'),
        client_secret=SPOTIFY.get('CLIENT_SECRET'),
        redirect_uri=SPOTIFY.get('REDIRECT_URI'),
        scope=SPOTIFY.get('ACCESS_SCOPE'),
    )

    def post(self, request):
        user = request.user

        if "playlist_id" not in request.data:
            raise ParseError
        playlist_id = request.data["playlist_id"]

        try:
            emotion_entity = EmotionEntityModel.objects.get(playlist_id=playlist_id)
        except:
            raise NotFound

        token_info = self.sp_oauth.refresh_access_token(user.refresh_token)
        sp = spotipy.Spotify(auth=token_info['access_token'])

        tracks_id = [track['track']['id'] for track in sp.playlist_tracks(playlist_id)['items']]

        playlist = sp.user_playlist_create(
            user=user.spotify_id,
            name=f"{random.choice(EMOTION_SYNONYMS[Emotion(emotion_entity.emotion)])} {random.choice(ANIMAL_NAMES)}",
            description=f"This playlist is made by InvisibleKids in {datetime.datetime.now().strftime('%X %d %B %Y')}.",
            public=False
        )
        sp.user_playlist_add_tracks(user=user.spotify_id, playlist_id=playlist['id'], tracks=tracks_id)

        return Response(status=status.HTTP_200_OK)


class LikeTrack(APIView):
    permission_classes = [IsAuthenticated]

    sp_oauth = spotipy.oauth2.SpotifyOAuth(
        client_id=SPOTIFY.get('CLIENT_ID'),
        client_secret=SPOTIFY.get('CLIENT_SECRET'),
        redirect_uri=SPOTIFY.get('REDIRECT_URI'),
        scope=SPOTIFY.get('ACCESS_SCOPE'),
    )

    def post(self, request):
        user = request.user

        if "playlist_id" not in request.data or "track_index" not in request.data:
            raise ParseError
        playlist_id = request.data["playlist_id"]
        track_index = int(request.data["track_index"])

        token_info = self.sp_oauth.refresh_access_token(user.refresh_token)
        sp = spotipy.Spotify(auth=token_info['access_token'])

        tracks_id = [track['track']['id'] for track in sp.playlist_tracks(playlist_id)['items']]

        sp.current_user_saved_tracks_add([tracks_id[track_index]])
        FeedbackModel.objects.create(
            user=user,
            playlist_id=playlist_id,
            music=MusicModel.objects.get(spotify_id=tracks_id[track_index]),
            state='like'
        )

        return Response(status=status.HTTP_200_OK)

    def delete(self, request):
        # for dislike
        user = request.user

        if "playlist_id" not in request.data or "track_index" not in request.data:
            raise ParseError
        playlist_id = request.data["playlist_id"]
        track_index = int(request.data["track_index"])

        token_info = self.sp_oauth.refresh_access_token(user.refresh_token)
        sp = spotipy.Spotify(auth=token_info['access_token'])

        tracks_id = [track['track']['id'] for track in sp.playlist_tracks(playlist_id)['items']]

        FeedbackModel.objects.create(
            user=user,
            playlist_id=playlist_id,
            music=MusicModel.objects.get(spotify_id=tracks_id[track_index]),
            state='dislike'
        )

        return Response(status=status.HTTP_200_OK)


class LikedTracks(APIView):
    permission_classes = [IsAuthenticated]

    sp_oauth = spotipy.oauth2.SpotifyOAuth(
        client_id=SPOTIFY.get('CLIENT_ID'),
        client_secret=SPOTIFY.get('CLIENT_SECRET'),
        redirect_uri=SPOTIFY.get('REDIRECT_URI'),
        scope=SPOTIFY.get('ACCESS_SCOPE'),
    )

    def post(self, request):
        user = request.user

        if "playlist_id" not in request.data:
            raise ParseError
        playlist_id = request.data["playlist_id"]

        token_info = self.sp_oauth.refresh_access_token(user.refresh_token)
        sp = spotipy.Spotify(auth=token_info['access_token'])

        tracks_id = [track['track']['id'] for track in sp.playlist_tracks(playlist_id)['items']]
        liked_tracks_id = [feedback.music.spotify_id for feedback in
                           FeedbackModel.objects.filter(user=user, playlist_id=playlist_id, state='like')]
        disliked_tracks_id = [feedback.music.spotify_id for feedback in
                              FeedbackModel.objects.filter(user=user, playlist_id=playlist_id, state='dislike')]

        liked_tracks_index = []
        disliked_tracks_index = []
        for ind, track_id in enumerate(tracks_id):
            if track_id in liked_tracks_id:
                liked_tracks_index.append(ind)
            if track_id in disliked_tracks_id:
                disliked_tracks_index.append(ind)

        return Response({'liked_tracks_index': liked_tracks_index, 'disliked_tracks_index': disliked_tracks_index},
                        status=status.HTTP_200_OK)


class Preferences(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user_id = request.user
        preference_data = []
        results = MusicPreferenceModel.objects.filter(user=user_id)
        for result in results:
            preference_data.append({'emotion': result.emotion, 'happyOrSad': result.prefered_feeling,
                                    'calmOrEnergetic': result.prefered_type})
        return Response({'preference': preference_data}, status=status.HTTP_200_OK)

    def post(self, request):
        if "preferences" not in request.data:
            raise ParseError
        preferences = json.loads(request.data["preferences"])

        for preference in preferences:
            MusicPreferenceModel.objects.filter(user=request.user, emotion=preference['emotion']).delete()
            MusicPreferenceModel.objects.create(
                user=request.user,
                emotion=preference['emotion'],
                prefered_feeling=preference['happyOrSad'],
                prefered_type=preference['calmOrEnergetic']
            )

        return Response(status=status.HTTP_200_OK)

    def delete(self, request):
        MusicPreferenceModel.objects.filter(user=request.user).delete()
        return Response(status=status.HTTP_200_OK)
