# Generated by Django 3.0.4 on 2020-06-09 15:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('emotion_detection', '0012_musicpreference'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='MusicPreference',
            new_name='MusicPreferenceModel',
        ),
    ]
