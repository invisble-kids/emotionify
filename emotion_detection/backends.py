from django.contrib.auth.backends import BaseBackend

from emotion_detection.models import SpotifyUser


class SpotifyUserBackend(BaseBackend):
    def authenticate(self, request, **kwargs):
        spotify_id = kwargs['spotify_id']
        try:
            return SpotifyUser.objects.get(spotify_id=spotify_id)
        except SpotifyUser.DoesNotExist:
            return None

    def get_user(self, spotify_id):
        try:
            return SpotifyUser.objects.get(pk=spotify_id)
        except SpotifyUser.DoesNotExist:
            return None
